/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/

#include "MainWindow.h"
#include "ui_MainWindow.h"
#include <QFileDialog>
#include <Connection.h>
#include <QFile>
#include <QDateTime>
MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    connect(ui->pushButton,SIGNAL(clicked(bool)),this,SLOT(OnOpenDB()));
    connect(ui->pushButton_2, SIGNAL(clicked(bool)),this,SLOT(OnGenerate()));
    Connection::CreateInstance();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::OnOpenDB()
{
    QString sLocation = QFileDialog::getExistingDirectory(this);
    ui->lineEdit->setText(sLocation);
}

void MainWindow::OnGenerate()
{
    QDateTime o = QDateTime::currentDateTime();

    QFile oFile(o.toString("yyyymmmddHHMMss")+"_DBFields.h");
    oFile.open(QIODevice::ReadWrite);
    oFile.write("/***************************This is auto generated************************************\n");
    oFile.write("***************Kanishka Weerasekara (c)-- kkgweerasekara@gmail.com*******************\n");
    oFile.write("*************************************************************************************/\n");
    oFile.write("#ifndef __DB_FIELDS__ \n#define __DB_FIELDS__ \n");
    Connection::GetInstance()->Initialize(ui->lineEdit->text(),"",false);
    QStringList oTables = Connection::GetInstance()->GetPrimaryTables();
    //QString sGName = QString("namespace ") + ui->txt_GlobalName->text().toUpper() + QString("\n{ \n");
    //oFile.write(qPrintable(sGName));
    foreach(QString sTable, oTables)
    {
        QString sTabNotice = QString("//Table name for ") + sTable.toUpper() + "\n";
        oFile.write(qPrintable(sTabNotice));
        //QString sNamespace = QString("namespace ") + sTable.toUpper() + QString(" \n{\n");
        QString sNamespace = QString("//Table fields for ") + sTable.toUpper() + QString(" \n");
        QString sTableName = QString("#define TBLNM_")+ui->txt_GlobalName->text().toUpper()+QString("_")+sTable.toUpper()+"\t" + "\""+sTable + "\" \n";
        oFile.write(qPrintable(sTableName));
        oFile.write( qPrintable(sNamespace));
        QStringList sFields = Connection::GetInstance()->GetTableColumns(sTable);
        foreach(QString sColumn, sFields)
        {
            QString sField = QString("#define FDNM_")+ui->txt_GlobalName->text().toUpper() +QString("_")+ sTable.toUpper()+ QString("_") +sColumn.toUpper() + QString(" \t \"") + sColumn + QString("\"\n");
            oFile.write(qPrintable(sField));
        }
        //}

        //oFile.write("\n} \n" );
    }
    //oFile.write("\n}\n");
     oFile.write("#endif //  __DB_FIELDS__ \n");
}
