/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/
#ifndef CONNECTION_H
#define CONNECTION_H
#include <QSqlDatabase>
#include <QList>
#include <QMap>
#include <QVariant>
#include <QStringList>
#include <memory>

#define _LOG_SQL_ ""
class Connection
{
public:
    static void CreateInstance();
    bool Initialize(QString sLocation, QString sSecondaryLocation,bool bReplicate);
    static void CloseDB();
    static Connection* GetInstance(){return p_Connection;}
    virtual ~Connection();
    //Sql query that will not return a data set: ex: insert query
    void RunQuery(QString sQuery);
    //SQL Query that will return data: ex: select query
    //function is bound with the entity object for simplicity
    //Idial implementation would be to use an intermediate object to translate between Entity and the database structures


    void UpdateQuery(QString sTableName, QMap<QString, QVariant> mapData, QString sKeyField = "", QString sValue = "");

    void InsertQuery(QString sTableName, QMap<QString, QVariant> mapData);

    bool DoesPrimaryKeyExists(QString sTableName, QString sFieldName, QVariant oValue);
    //Return column names of a table
    QStringList GetTableColumns(QString sTableName);
    //Delete
    void DeleteRow(QString sTable, QString sKey, QString sValue);
    //Verify that both databases contain all tables and that row counts are similar//
    //Verification does not check each and every data item.
    bool VerifyDBs();
    //*** Not recomanded to use for writing since this will not write data to both databases.

    QStringList GetPrimaryTables();
    QSqlDatabase* GetPrimaryDb(){return &db_Primary;}
    QString AppendDataToQuery(QString sFieldName, QVariant oValue, QString sTableName);

private:
    Connection();
    static Connection* p_Connection;

     QSqlDatabase db_Primary;
     QSqlDatabase db_Replica;
     bool b_ReplicateDB;
};

#endif // CONNECTION_H
