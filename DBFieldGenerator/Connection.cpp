#include "Connection.h"
#include <QDebug>
#include <QSqlQuery>


#include <QSqlRecord>
#include <QSqlField>

#include <QSqlIndex>
#include <QSqlError>
Connection* Connection::p_Connection;
//QSqlDatabase Connection::db;

void Connection::CreateInstance()
{
     p_Connection = new Connection();

}

bool Connection::Initialize(QString sLocation, QString sSecondaryLocation, bool bReplicate)
{
    b_ReplicateDB = bReplicate;
    db_Primary = QSqlDatabase::addDatabase("QSQLITE","Primary");
    sLocation+="/Application";
    db_Primary.setDatabaseName(sLocation);
    bool db_ok = db_Primary.open();
    qDebug() << "DB Open" << db_ok ;
    bool db_Rep_Ok = true;
    if(b_ReplicateDB)
    {
        db_Replica = QSqlDatabase::addDatabase("QSQLITE","Secondary");
        sSecondaryLocation+="/Application";
        db_Replica.setDatabaseName(sSecondaryLocation);
        bool db_Rep_Ok = db_Replica.open();
        qDebug() << "Rep DB open" << db_Rep_Ok;
    }

    return (db_ok && db_Rep_Ok);
}

void Connection::CloseDB()
{
    //db.removeDatabase("QSQLITE");
    //db.close();
}

Connection::~Connection()
{

    db_Primary.removeDatabase("QSQLITE");
    db_Primary.close();

    if(b_ReplicateDB)
    {
        db_Replica.removeDatabase("QSQLITE");
        db_Replica.close();
    }
qDebug() << "DB closed";
    //db.close();

}

void Connection::RunQuery(QString sQuery)
{
    QSqlQuery oQuery(sQuery,db_Primary);
    //oQuery.exec();

    if(b_ReplicateDB)
    {
        QSqlQuery oQueryR(sQuery,db_Replica);
        //oQueryR.exec();
    }
}

void Connection::UpdateQuery(QString sTableName, QMap<QString, QVariant> mapData, QString sKeyField, QString sValue)
{
    if(mapData.size() < 1)
        return;

    QSqlRecord oRecorLogi = db_Primary.record(sTableName);
    QString sQuery= QString("update %1 set  ").arg(sTableName);
    for(QMap<QString,QVariant>::iterator itr = mapData.begin(); itr != mapData.end(); itr++)
    {
        QString sField = itr.key();
        QVariant oData = itr.value();
        QSqlField oField = oRecorLogi.field(sField);
        QVariant::Type enType = oField.type();
        sQuery += sField ;
        sQuery+= " = ";
        if(enType == QVariant::String)
        {
            sQuery+=" '";
        }
        sQuery+= oData.toString();
        if(enType == QVariant::String)
        {
            sQuery+="' ";
        }
    }
    if(sKeyField != QString(""))
    {

        sQuery += QString(" where %1 = '%2'").arg(sKeyField).arg(sValue);
    }
    RunQuery(sQuery);
}

void Connection::InsertQuery(QString sTableName, QMap<QString, QVariant> mapData)
{
    if(mapData.size() < 1)
        return;

    QSqlRecord oRecorLogi = db_Primary.record(sTableName);
    QString sQuery= QString("insert into %1 ").arg(sTableName);
    QStringList sColumns;
    QStringList sValues;
    for(QMap<QString,QVariant>::iterator itr = mapData.begin(); itr != mapData.end(); itr++)
    {
        QString sField = itr.key();
        QVariant oData = itr.value();
        QSqlField oField = oRecorLogi.field(sField);
        QVariant::Type enType = oField.type();
        QString sValue;

        if(enType == QVariant::String)
        {
            sValue+=" '";
        }
        sValue+= oData.toString();
        if(enType == QVariant::String)
        {
            sValue+="' ";
        }
        sColumns.push_back(sField);
        sValues.push_back(sValue);
    }
    sQuery+= QString(" ( ") += sColumns.join(",")+= QString(" )");
    sQuery+= "values";
    sQuery+=QString("( ") += sValues.join(",")+=" ) ";
#ifdef _LOG_SQL_
    qDebug() << sQuery;
#endif

    RunQuery(sQuery);
}

QString Connection::AppendDataToQuery(QString sFieldName, QVariant oValue, QString sTableName)
{
    QSqlRecord oRecord = db_Primary.record(sTableName);
    QSqlField oField = oRecord.field(sFieldName);
    QVariant::Type enType = oField.type();
    QString sValue;

    if(enType == QVariant::String)
    {
        sValue+=" '";
    }
    sValue+= oValue.toString();
    if(enType == QVariant::String)
    {
        sValue+="' ";
    }

    return sValue;
}

bool Connection::DoesPrimaryKeyExists(QString sTableName, QString sFieldName, QVariant oValue)
{
    QString sQuery = QString("select count(*)  from %1 where %2 = ").arg(sTableName).arg(sFieldName);
    QString sValue = AppendDataToQuery(sFieldName, oValue, sTableName);
    sQuery+=sValue;
    QSqlQuery oQuery(sQuery,db_Primary);
    oQuery.first();

    int iPrimarySize = oQuery.value(0).toInt();

     return (iPrimarySize > 0);
}

QStringList Connection::GetTableColumns(QString sTableName)
{
    QSqlRecord oNames = db_Primary.record(sTableName);
    QStringList oList;
    int iCount = oNames.count();
    for(int i = 0 ;i < iCount; i++)
    {
        QString sField;
        QSqlField oField = oNames.field(i);
        oList.push_back(oField.name());
    }
    return oList;
}

void Connection::DeleteRow(QString sTable, QString sKey, QString sValue)
{
    QString sQuery = QString("delete from %1 where %2 = ").arg(sTable).arg(sKey);
    sQuery += AppendDataToQuery(sKey,QVariant(sValue),sTable);
    RunQuery(sQuery);
}

bool Connection::VerifyDBs()
{
    if(!b_ReplicateDB)
        return false;

   QStringList sPrimaryTables =  db_Primary.tables();
   QStringList sSecondaryTables = db_Replica.tables();
   foreach(QString sTable, sPrimaryTables)
   {
       if(!sSecondaryTables.contains(sTable))
       {
           QString sLog = QString("Secondary does not contain table %1").arg(sTable);

           return false;
       }
       QString sQuery = QString("select count(*) from %1").arg(sTable);
       QSqlQuery oPrimaryRecords = QSqlQuery(sQuery,db_Primary);
       QSqlQuery oSecondaryRecords = QSqlQuery(sQuery,db_Replica);
       oPrimaryRecords.exec();
       oSecondaryRecords.exec();
       oPrimaryRecords.first();
       oSecondaryRecords.first();
       int iPrimarySize = oPrimaryRecords.value(0).toInt();
       int iSecondarySize = oSecondaryRecords.value(0).toInt();
       if(iPrimarySize != iSecondarySize)
       {
           QString sLog = QString("Secondary does not match in count to primary %1").arg(sTable);

           return false;
       }
       else
       {
           QString sLog = QString("primary and secndary contains %1 for table %2").arg(oPrimaryRecords.value(0).toInt()).arg(sTable);

       }
   }

   return true;
}

QStringList Connection::GetPrimaryTables()
{
    return db_Primary.tables();
}

Connection::Connection()
{
     b_ReplicateDB = false;
}

