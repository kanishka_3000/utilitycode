#-------------------------------------------------
#
# Project created by QtCreator 2016-02-07T14:37:25
#
#-------------------------------------------------

QT       += core gui sql

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = DBFieldGenerator
TEMPLATE = app


SOURCES += main.cpp\
        MainWindow.cpp \
    Connection.cpp

HEADERS  += MainWindow.h \
    Connection.h

FORMS    += MainWindow.ui
