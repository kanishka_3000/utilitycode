#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QWidget>
#include <QFont>
namespace Ui {
class MainWindow;
}

class MainWindow : public QWidget
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
public slots:
    void OnSelectFont();
    void OnCalculate();
private:
    Ui::MainWindow *ui;
    QFont o_Font;

};

#endif // MAINWINDOW_H
