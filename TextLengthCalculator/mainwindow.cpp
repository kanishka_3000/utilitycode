#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QFontDialog>
MainWindow::MainWindow(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::OnSelectFont()
{
    bool bOk;
    o_Font = QFontDialog::getFont(&bOk,this);
    ui->lbl_FontInfo->setText(QString("Font- %1 Size- %2").arg(o_Font.family()).arg(o_Font.pointSize()));

}

void MainWindow::OnCalculate()
{
    QFontMetrics oFontMatrix(o_Font);
    int iSize = oFontMatrix.width(ui->txt_Text->text());
    ui->lbl_Value->setText(QString("%1").arg(iSize));
    ui->lbl_Value->setMinimumWidth(iSize);
    ui->lbl_Value->setMaximumWidth(iSize);
}
