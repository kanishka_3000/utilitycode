#include "LabelTextVerifier.h"
#include <QFontMetrics>
#include <QDebug>
LabelTextVerifier::LabelTextVerifier()
{
}

void LabelTextVerifier::VerifyTextOfLabel(QLabel *pLabel)
{
    QFont oLabelFont = pLabel->font();
    QString sLabelText = pLabel->text();

    QFontMetrics oMetrx(oLabelFont);
    int iWidth = pLabel->width();
    int iRequidedWidth = oMetrx.width(sLabelText);
    if(iWidth < iRequidedWidth)
    {
        qDebug() << "Size Not Sufficient" << sLabelText << " " << iWidth << " " << iRequidedWidth;
    }

}

void LabelTextVerifier::VerifyAllChildren(QWidget *pParent)
{
    QList<QLabel*> lstChildren = pParent->findChildren<QLabel*>();
    foreach(QLabel* pLabel, lstChildren)
    {
        VerifyTextOfLabel(pLabel);
    }
}
