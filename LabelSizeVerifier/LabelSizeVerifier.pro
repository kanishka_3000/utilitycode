#-------------------------------------------------
#
# Project created by QtCreator 2015-06-09T19:21:16
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = LabelSizeVerifier
TEMPLATE = app


SOURCES += main.cpp\
        testwindow.cpp \
    LabelTextVerifier.cpp

HEADERS  += testwindow.h \
    LabelTextVerifier.h

FORMS    += testwindow.ui
