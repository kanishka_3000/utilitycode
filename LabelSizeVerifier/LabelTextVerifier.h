#ifndef LABELTEXTVERIFIER_H
#define LABELTEXTVERIFIER_H
#include <QLabel>
class LabelTextVerifier
{
public:
    LabelTextVerifier();
    void VerifyTextOfLabel(QLabel* pLabel);
    void VerifyAllChildren(QWidget* pParent);
};

#endif // LABELTEXTVERIFIER_H
