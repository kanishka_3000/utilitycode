#include "testwindow.h"
#include "ui_testwindow.h"
#include <LabelTextVerifier.h>
TestWindow::TestWindow(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::TestWindow)
{
    ui->setupUi(this);
    LabelTextVerifier oVerify;
    oVerify.VerifyAllChildren(this);
}

TestWindow::~TestWindow()
{
    delete ui;
}
